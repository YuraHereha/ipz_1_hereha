@startuml
: Відкрити форму повернення білету;
: Вказати причину повернення;
if (Сервер підтвердив повернення) then (так)
:Вікно з успішно виконаною операцією;
else (ні) 
:Повторне надсилання запиту;
if (Перевірка успішності операції) then (Успішна)
else (Помилка)
:Повторити спробу через деякий час;
endif
endif

:Вивести на головний екран;
@enduml
