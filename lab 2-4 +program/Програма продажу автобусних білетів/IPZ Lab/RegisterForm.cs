﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IPZ_Lab
{
    public partial class userNameDield : Form
    {
        public object MessegeBox { get; private set; }

        public userNameDield()
        {
            InitializeComponent();

            userNameField.Text = "Введіть ім'я";
            userNameField.ForeColor = Color.Gray;

            NF2.Text = "Введіть логін";
            NF2.ForeColor = Color.Gray;

            passbox.Text = "Введіть прізвище";
            passbox.ForeColor = Color.Gray;

            passbox2.Text = "Введіть пароль";
            passbox2.ForeColor = Color.Gray;




        }



        private void userNameField_Enter(object sender, EventArgs e)
        {
            if (userNameField.Text == "Введіть ім'я")

            {
                userNameField.Text = "";
                userNameField.ForeColor = Color.Black;
            }
        }

        private void userNameField_Leave(object sender, EventArgs e)
        {
            if (userNameField.Text == "")
            {
                userNameField.Text = "Введіть ім'я";
                userNameField.ForeColor = Color.Gray;
            }
        }

        private void passbox_Enter(object sender, EventArgs e)
        {
            if (passbox.Text == "Введіть прізвище")

            {
                passbox.Text = "";
                passbox.ForeColor = Color.Black;
            }
        }

        private void passbox_Leave(object sender, EventArgs e)
        {
            if (passbox.Text == "")
            {
                passbox.Text = "Введіть прізвище";
                passbox.ForeColor = Color.Gray;
            }
        }

        private void NF2_Enter(object sender, EventArgs e)
        {
            if (NF2.Text == "Введіть логін")

            {
                NF2.Text = "";
                NF2.ForeColor = Color.Black;
            }
        }

        private void NF2_Leave(object sender, EventArgs e)
        {
            if (NF2.Text == "")
            {
                NF2.Text = "Введіть логін";
                NF2.ForeColor = Color.Gray;
            }
        }

        private void passbox2_Enter(object sender, EventArgs e)
        {
            if (passbox2.Text == "Введіть пароль")

            {
                passbox2.Text = "";
                passbox2.ForeColor = Color.Black;
            }
        }

        private void passbox2_Leave(object sender, EventArgs e)
        {
            if (passbox2.Text == "")
            {
                passbox2.Text = "Введіть пароль";
                passbox2.ForeColor = Color.Gray;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (userNameField.Text == "Введіть ім'я")
            {
                MessageBox.Show("Введіть ім'я");
                return;
            }

            if (passbox2.Text == "Введіть пароль")
            {
                MessageBox.Show("Введіть пароль");
                return;
            }

            if (NF2.Text == "Введіть логін")
            {
                MessageBox.Show("Введіть логін");
                return;
            }

            if (passbox.Text == "Введіть прізвище")
            {
                MessageBox.Show("Введіть прізвище");
                return;
            }


            if (isUserExist())
                return;

            DB db = new DB();
            MySqlCommand command = new MySqlCommand("INSERT INTO `users` (`login`, `pass`, `name`, `surname`) VALUES (@login, @pass,@name, @surname)", db.GetConnection());

            command.Parameters.Add("@login", MySqlDbType.VarChar).Value = NF2.Text;
            command.Parameters.Add("@pass", MySqlDbType.VarChar).Value = passbox2.Text;
            command.Parameters.Add("@name", MySqlDbType.VarChar).Value = userNameField.Text;
            command.Parameters.Add("@surname", MySqlDbType.VarChar).Value = passbox.Text;

            db.openConnection();

            if (command.ExecuteNonQuery() == 1)
                MessageBox.Show("Аккаунт Створено");
                
            else
                MessageBox.Show("Аккаунт не Створено");


            db.closeConnection();

        }


        public Boolean isUserExist()
        {
            DB db = new DB();

            DataTable table = new DataTable();

            MySqlDataAdapter adapter = new MySqlDataAdapter();

            MySqlCommand command = new MySqlCommand("SELECT * FROM `users` WHERE `login` = @uL", db.GetConnection());
            command.Parameters.Add("@uL", MySqlDbType.VarChar).Value = NF2.Text;

            adapter.SelectCommand = command;
            adapter.Fill(table);

            if (table.Rows.Count > 0)
            {
                MessageBox.Show("Користувач з таким логіном уже зареєстрований");
                return true;
            }

            else
                return false;
        }

        private void label2_Click(object sender, EventArgs e)
        {
            this.Hide();
            LoginForm loginform = new LoginForm();
            loginform.Show();
        }

        private void userNameDield_Load(object sender, EventArgs e)
        {

        }
    }
}
