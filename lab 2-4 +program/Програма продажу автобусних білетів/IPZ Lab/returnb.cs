﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IPZ_Lab
{
    public partial class returnb : Form
    {
        SqlConnection sqlConnection;
        public returnb()
        {
            InitializeComponent();
        }

        private async void returnb_Load(object sender, EventArgs e)
        {
            string connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\gereg\source\repos\IPZ Lab\IPZ Lab\Database.mdf;Integrated Security=True";

            sqlConnection = new SqlConnection(connectionString);

            await sqlConnection.OpenAsync();

            SqlDataReader sqlReader = null;
            SqlCommand command = new SqlCommand("SELECT * FROM [Products]", sqlConnection);


            try
            {
                sqlReader = await command.ExecuteReaderAsync();
                while (await sqlReader.ReadAsync())
                {
                    listBox1.Items.Add(Convert.ToString(sqlReader["id"] + " " + Convert.ToString(sqlReader["Name"]) + " " + Convert.ToString(sqlReader["Price"])));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), ex.Source.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            finally
            {
                if (sqlReader != null)
                    sqlReader.Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            mainU mainU = new mainU();
            mainU.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != String.Empty)
            {
                MessageBox.Show("Запит на повернення одобрено\nПідтримка зв'яжеться з Вами через пошту");
            }
            else
            {
                MessageBox.Show("Введіть ID білету \nякий бажаєте повернути");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            returnb returnb = new returnb();
            returnb.Show();
        }
    }
}
